package ejercito;


public class Recluta extends Soldado{
    static int numRecluta = 0;
    String idRecluta;
    
    public Recluta(String nombre, int edad, String rango) {
        super(nombre,edad,rango);
        numRecluta++;
        idRecluta = "Recluta" + numRecluta;
    }

    public static int getNumRecluta() {
        return numRecluta;
    }

    public static void setNumRecluta(int numRecluta) {
        Recluta.numRecluta = numRecluta;
    }

    public String getIdRecluta() {
        return idRecluta;
    }

    public void setIdRecluta(String idRecluta) {
        this.idRecluta = idRecluta;
    }

    @Override
    public String toString() {
        return "Recluta{" + "Nombre: " + nombre + ", Edad: " + edad + ", Rango: " + rango + " Id: " +  idRecluta + '}';
    }
    
    
    
    
}
