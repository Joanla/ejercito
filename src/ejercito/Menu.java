package ejercito;
import java.util.LinkedList;
import java.util.Scanner;


public class Menu {
    LinkedList<Recluta> reclutas = new LinkedList<>();
    
    public static void main(String[] args) {
        Menu m1 = new Menu();
        Scanner sc = new Scanner(System.in);
        
        String nombre;
        int edad;
        String rango;
        int dec=0;
        String decS;
        String id="";
        
         Recluta r2 = new Recluta("José",26,"Capitan");
         m1.addRecluta(r2);
          Recluta r3 = new Recluta("Paco",23,"Recluta");
         m1.addRecluta(r3);
          Recluta r4 = new Recluta("Javi",53,"Sargento");
         m1.addRecluta(r4);
       
        System.out.println("Bienvenido al registro de reclutas del ejercito de paz DAM.");
        System.out.println("");
        do{
        dec=0;
        while (dec<1 || dec>4) {
        System.out.println("¿Que desea hacer?");
        System.out.println("");
        System.out.println("");
        System.out.println("1- Añadir recluta.");
        System.out.println("2- Borrar recluta.");
        System.out.println("3- Modificar recluta.");
        System.out.println("4- Mostrar listado de reclutas.");
        dec=sc.nextInt();
        }
            switch (dec) {

                case 1:
                    System.out.print("Dime su nombre: ");
                    nombre = sc.next();
                   
                    System.out.print("Dime su edad: ");
                    edad = sc.nextInt();

                    System.out.print("Dime su rango: ");
                    rango = sc.next();

                    Recluta r1 = new Recluta(nombre,edad,rango);
                    m1.addRecluta(r1);
                    break;

                case 2:
                    m1.mostrarRecluta();
                    System.out.println("ATENCIÓN");
                    System.out.println("Los reclutas se borran a través del ID. Por ejemplo, si quisiera borrar el recluta con el ID"
                            + " 'Recluta3', introduciría ese mismo ID.");
                    System.out.println("¿Que recluta deseas eliminar?");
                    id=sc.next();
                   
                    m1.eliminarRecluta(id);
                    break;
                case 3:
                    System.out.println("ATENCIÓN");
                    System.out.println("Los reclutas se modifican a través del ID. Por ejemplo, si quisiera modificar el recluta con el ID"
                            + " 'Recluta3', introduciría ese mismo ID.");
                    m1.mostrarRecluta();
                    m1.modificarRecluta(id);
                case 4:
                   
                    m1.mostrarRecluta();
                    break;
            }       
            
            decS="";
            while (!decS.equals("s")){
            System.out.println("¿Desea hacer algo más? s/n");
            decS=sc.next();
            
                if (decS.equals("n")) {
                    break;
                }
            }
            
        } while (decS.equals("s"));
        
       
    }
    
    public void addRecluta(Recluta r) {
        reclutas.add(r);
    }
    
    public void mostrarRecluta() {
        for (int i = 0; i < reclutas.size(); i++) {
            System.out.println(reclutas.get(i));
        }
    }
    
    public void eliminarRecluta(String  id) {
       boolean existe = false;
        
        
        for (int i = 0; i < reclutas.size(); i++) {
            if (id.equals(reclutas.get(i).getIdRecluta())) {
                reclutas.remove(reclutas.get(i));
                existe = true;
                break;
            } 
        }
        
        if (existe == false){
            System.out.println("No existe el recluta.");
        }
    }
    
    public void modificarRecluta(String id) {
        Scanner sc = new Scanner(System.in);
        String nombre = "";
        int op = 0;
        int cont=0;
        do {
            if (cont>0) {
            System.out.println("El ID " + nombre + " no existe.");
            }
            cont++;
            System.out.println("¿Que recluta deseas modificar?");
            nombre = sc.nextLine();
            comprobarRecluta(nombre);
        } while (comprobarRecluta(nombre)==false);
        while (op<1 || op>3){
            System.out.println("¿Que deseas hacer?");
            System.out.println("1- Modificar nombre.");
            System.out.println("2- Modificar edad.");
            System.out.println("3- Modificar rango.");
            op = sc.nextInt();
        }
            menuModificar(op, nombre);
        
    }
    
    public void modNombre(String id, String nombre) {
        for (int i = 0; i < reclutas.size(); i++) {
            if (id.equals(reclutas.get(i).getIdRecluta())) {
                reclutas.get(i).setNombre(nombre);
                break;
            }
        }
    }
    
    public void modEdad(String id, int edad) {
        for (int i = 0; i < reclutas.size(); i++) {
            if (id.equals(reclutas.get(i).getIdRecluta())) {
                reclutas.get(i).setEdad(edad);
                break;
            }
        }
    }
    
    public void modRango(String id, String rango) {
        for (int i = 0; i < reclutas.size(); i++) {
            if (id.equals(reclutas.get(i).getIdRecluta())) {
                reclutas.get(i).setRango(rango);
                break;
            }
        }
    }
    
    
    public boolean comprobarRecluta(String nombre) {
        boolean existe = false;
        for (int i = 0; i < reclutas.size(); i++) {
            if (nombre.equals(reclutas.get(i).getIdRecluta())) {
                existe = true;
            }
        }
       
        return existe;
    }
    
    public void menuModificar(int op, String id) {
        Scanner sc = new Scanner(System.in);
        switch (op) {
            case 1:
                String nombre = "";
                System.out.println("Introduzca el nuevo nombre.");
                nombre = sc.nextLine();
                modNombre(id, nombre);
                System.out.println("Se ha cambiado el nombre correctamente.");
                break;
            case 2:
                int edad = 0;
                System.out.println("Introduzca la nueva edad");
                edad = sc.nextInt();
                modEdad(id, edad);
                System.out.println("Se ha cambiado la edad correctamente.");
                break;
            case 3:
                String rango = "";
                System.out.println("Introduzca el nuevo rango");
                rango = sc.nextLine();
                modRango(id, rango);
                System.out.println("Se ha cambiado el rango correctamente.");
                break;
        }
    }
}
